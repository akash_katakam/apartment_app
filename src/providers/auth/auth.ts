import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore'

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor() {}
  loginUser(email: string, password: string): Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(email,password);
  }
  logoutUser():Promise<void> {
    return firebase.auth().signOut();
  }
  signupUser(email: string, password: string ,data:any): Promise<any> {
    return firebase
      .auth()
      .createUserWithEmailAndPassword(email, password).then(value => {
          let id = value.user.uid;
          firebase.firestore().collection('users').doc(id).set(data);
        });
  }
}
