import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database'
import 'firebase/firestore'

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  apartments : any = [];
  constructor() {
    //this.getAllApartments();
  }

  getAllApartments () : Promise<any> {
    //firebase.firestore().collection("apartments").doc('');
    return firebase.firestore().collection("apartments").get().then(
      value => {
        console.log(value);
        value.forEach(document=> {
          console.log(document.id);
          this.apartments.push(document.data());
        });
        console.log(this.apartments);
        return this.apartments;
      });
  }

}
