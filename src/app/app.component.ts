import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import {LoginPage} from '../pages/login/login'
import firebase from 'firebase/app';
import { firebaseConfig } from './config';
import 'firebase/auth';
import {AuthProvider} from "../providers/auth/auth";
import {ClubHousePage} from "../pages/club-house/club-house";
import {VisitorsPage} from "../pages/visitors/visitors";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  rootPage:any ;
  @ViewChild (Nav) nav: Nav;
  pages: Array<{title : string;component : any}>;
  app : firebase.app.App;
  db: firebase.database.Database;
  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public authProvider: AuthProvider) {
    this.pages =[
      {title : "Announcements",component : ""},
      {title : "Groups",component : ""},
      {title : "Club House",component : ClubHousePage},
      {title : "Security",component : ""},
      {title : "Visitors",component : VisitorsPage},
      {title : "Book PlayGrounds",component : ""},
      {title : "Complaints",component : ""}
    ];
    this.initialiseApp();
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (!user) {
        this.rootPage = LoginPage;
        unsubscribe();
      } else {
        this.rootPage = TabsControllerPage;
        console.log(user);
        unsubscribe();
      }
    });
  }
  initialiseApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    firebase.initializeApp(firebaseConfig);


  }

  logOutUser() {
    this.authProvider.logoutUser();
  }
  openPage(page : {title : string , component :any}){
    this.nav.push(page.component);
  }
}



