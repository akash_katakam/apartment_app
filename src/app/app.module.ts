import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler ,NavController} from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { AlexaPage } from '../pages/alexa/alexa';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { MaintenancePage } from '../pages/maintenance/maintenance';
import { VisitorsPage } from '../pages/visitors/visitors';
import { ClubHousePage } from '../pages/club-house/club-house';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import {CreateUserPage} from "../pages/create-user/create-user";
import { DatabaseProvider } from '../providers/database/database';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    AlexaPage,
    TabsControllerPage,
    LoginPage,
    SignupPage,
    MaintenancePage,
    VisitorsPage,
    ClubHousePage,
    CreateUserPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HomePage,
    AlexaPage,
    TabsControllerPage,
    LoginPage,
    SignupPage,
    MaintenancePage,
    VisitorsPage,
    ClubHousePage,
    CreateUserPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    DatabaseProvider
  ]
})
export class AppModule {}
