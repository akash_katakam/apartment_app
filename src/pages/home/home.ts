import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MaintenancePage } from '../maintenance/maintenance';
import { ClubHousePage } from '../club-house/club-house';
import { VisitorsPage } from '../visitors/visitors';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  constructor(public navCtrl: NavController) {
  }
  goToMaintenance(params){
    if (!params) params = {};
    this.navCtrl.push(MaintenancePage);
  }goToClubHouse(params){
    if (!params) params = {};
    this.navCtrl.push(ClubHousePage);
  }goToVisitors(params){
    if (!params) params = {};
    this.navCtrl.push(VisitorsPage);
  }
}
