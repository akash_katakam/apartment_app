import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthProvider} from "../../providers/auth/auth";
import {TabsControllerPage} from "../tabs-controller/tabs-controller";
import {CreateUserPage} from "../create-user/create-user";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loginForm: any;
  // this tells the tabs component which Pages
  // should be each tab's root Page
  constructor(public navCtrl: NavController,public authProvider: AuthProvider, formBuilder: FormBuilder) {
    //TODO: add validations
    this.loginForm = formBuilder.group({
      email: [''],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ]
    });
  }
  loginUser() {
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    this.authProvider.loginUser(email,password).then(
      authData => {this.navCtrl.setRoot(TabsControllerPage);
        console.log(authData)},
      error => {
        alert("invalid credentials");
        this.navCtrl.setRoot(LoginPage);
      });
  }

  goToSignup() {
    this.navCtrl.push(CreateUserPage);
  }
}
