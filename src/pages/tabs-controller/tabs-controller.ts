import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlexaPage } from '../alexa/alexa';
import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = AlexaPage;
  tab3Root: any = AboutPage;
  constructor(public navCtrl: NavController) {
  }
  goToAlexa(params){
    if (!params) params = {};
    this.navCtrl.push(AlexaPage);
  }goToAbout(params){
    if (!params) params = {};
    this.navCtrl.push(AboutPage);
  }
}
