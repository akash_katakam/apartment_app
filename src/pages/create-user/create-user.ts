import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase/app';
import "firebase/database";
import {AuthProvider} from "../../providers/auth/auth";
import {DatabaseProvider} from "../../providers/database/database";
import {Apartment} from "../../model/apartment";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
//import "firebase/app"

/**
 * Generated class for the CreateUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-create-user',
  templateUrl: 'create-user.html',
})
export class CreateUserPage {
  apartments: [{}];
  selectedApartment :any;
  signUpForm: FormGroup;
  //TODO: add validations
  constructor(public navCtrl: NavController,
              public databaseProvider : DatabaseProvider,
              public authProvider : AuthProvider,
              formBuilder: FormBuilder) {
    this. signUpForm = formBuilder.group({
      email: [''],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ],
      name: [''],
      confirmPassword : ['']
    });
    databaseProvider.getAllApartments().then(value => this.apartments=value);
  }

  signUpUser() {
    const email = this.signUpForm.value.email;
    const password = this.signUpForm.value.password;
    console.log(this.selectedApartment);
    this.authProvider.signupUser(email,password,{'apartmentId' : Number.parseInt(this.selectedApartment)});
  }
  toggleApartment() {

  }
}
